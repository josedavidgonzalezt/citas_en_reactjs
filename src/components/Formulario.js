import React, {Fragment,useState}from 'react';
import uuid from 'uuid/v4';
const Formulario = ({crearCita}) => {
// State de Citas
const [cita, actualizarCita]=useState({
    mascota:'',
    propietario:'',
    telefono:'',
    fecha:'',
    hora:'',
    sintomas:'',
});

//state de error
const [error,actualizarError]=useState(false);
// Funcion que se ejecuta cada vez que el usuario escribe en un input
const actualizarState =(e)=>{
    actualizarCita ({
        ...cita,
        [e.target.name] : e.target.value
})
}
//extraer valores
const {mascota,propietario,telefono,fecha,hora,sintomas}=cita;

// Cuando el usuario preciona agregar cita
const submitCita = (e)=>{
    e.preventDefault();

    //Validación del form
     if(mascota.trim()==='' || propietario.trim()==='' || telefono.trim()==='' ||fecha.trim()==='' || hora.trim()==='' || sintomas.trim()===''){
         actualizarError(true);
         console.log('campo vacio');
         return;
     }
     
     //Eliminar mensaje previo
     actualizarError(false); 
console.log('agregando')

//ASIGNAR UN ID: instalamos una libreria llamada UUid, EN LA CONSOLA COLOCAMOS npm i uuid
cita.id=uuid();

//Crear Cita
crearCita(cita);

//Reiniciar Form
actualizarCita({
    mascota:'',
    propietario:'',
    telefono:'',
    fecha:'',
    hora:'',
    sintomas:'',
})

}


    return ( 
        <Fragment>
            <h2>Crear Citas</h2>
            <form 
            onSubmit={submitCita}
            >
            {error
            ? <p className="alerta-error">Todos los campos son necesarios</p>
            :null
            
            }
              <label>Nombre de la Mascota</label>
              <input
                 type="text"
                 name="mascota"
                 className="u-full-width"
                 placeholder="Ejemplo: Pegasus"
                 onChange={actualizarState}
                 value={mascota}
              />

              <label>Nombre del Dueño</label>
              <input
                 type="text"
                 name="propietario"
                 className="u-full-width"
                 placeholder="Ejemplo: Evan González"
                 onChange={actualizarState}
                 value={propietario}
              />
            <label>Celular</label>
              <input
                 type="tel"
                 name="telefono"
                 className="u-full-width"
                 onChange={actualizarState}
                 value={telefono}
              />


             <label>Fecha:</label>
              <input
                 type="date"
                 name="fecha"
                 className="u-full-width"
                 onChange={actualizarState}
                 value={fecha}
              />

               <label>Hora:</label>
              <input
                 type="time"
                 min="10:00"
                 max="17:00"
                 name="hora"
                 className="u-full-width"
                 onChange={actualizarState}
                 value={hora}
              />

              <label>Síntomas</label>
              <textarea
              className="u-full-width"
              name="sintomas"
              placeholder="Ejemplo: Mi mascota presenta perdida de pelo y..."
              onChange={actualizarState}
              value={sintomas}
              ></textarea>

              <button
              type="submit"
              className="u-full-width button-primary"
              
              >Agregar Cita</button>

              
            </form>

        </Fragment>
     );
}
 
export default Formulario;