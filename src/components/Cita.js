import React from 'react';
const Cita = ({cita,eliminarCita}) => {
    return ( 

        <div className="cita">
            <p>Mascota: <span>{cita.mascota}</span></p>
            <p>Propietario: <span>{cita.propietario}</span></p>
            <p>telefono: <span>{cita.telefono}</span></p>
            <p>Fecha: <span>{cita.fecha}</span></p>
            <p>Hora: <span>{cita.hora}</span></p>
            <p>Sintomas: <span>{cita.sintomas}</span></p>
            <button
            type="button"
            className="button eliminar u-full-width"
            onClick={()=>eliminarCita(cita.id)}
            
            > &times; Eliminar Cita</button>
        </div>
     );
}
 
export default Cita;