import React, {Fragment,useState,useEffect} from 'react';
import Formulario from './components/Formulario';
import Cita from './components/Cita';


function App() {


//Citas en el local storage
let citasIniciales = JSON.parse(localStorage.getItem('citas'));
if(!citasIniciales){
  citasIniciales = [];
}

  //Arreglo de citas
const [citas,guardarCitas]= useState(citasIniciales);

// use effect para realizar ciertas operaciones cuando el state cambia
useEffect( ()=>{
  if(citasIniciales){
    localStorage.setItem('citas', JSON.stringify(citas))
  }
  else{
    localStorage.setItem('citas', JSON.stringify([]));
  }
}, [citas]);

//Funcion que tome la citas actuales y guarde la nueva
const crearCita = cita=>{
  guardarCitas([...citas, cita]);
}

//Funcion para eliminar la cita por su id
const eliminarCita = id =>{
  const nuevasCitas = citas.filter(cita => cita.id !==id);
  guardarCitas (nuevasCitas);
}

//condicional que cambia el titulo
const titulo = citas.length===0 ?'No hay citas' :'Administra tus citas' ; 
  return (
   <Fragment>
     <h1>Administrador de Pacientes</h1>
     <div className="container">
       <div className="row">
         <div className="one-half column contenedor">
           <Formulario
           crearCita={crearCita}
           />
         </div>
         <div className="one-half column contenedor">
           <h2>{titulo}</h2>
           {citas.map(cita=>(
             <Cita
             key={citas.id}
             cita={cita}
             eliminarCita={eliminarCita}
             />
             ))}
             
          </div>
       </div>
     </div>
   </Fragment>
  );
}

export default App;

